package Sumando;

import java.util.Scanner;

public class Sumando {

	public static void main(String[] args) {
	Scanner input = new Scanner(System.in);
	
	System.out.println("Introduce un numero");
	int numero1 = input.nextInt();
	
	System.out.println("Introduce otro numero");
	int numero2 = input.nextInt();
	
	int resultado = numero1 + numero2;
	System.out.println("El resultado es " + resultado);
	input.close();

	}

}
